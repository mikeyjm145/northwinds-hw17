﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ProductMaintenance.aspx.cs" Inherits="ProductMaintenance" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Morguarge Northwinds HW 17</title>
    <link href="App_Themes/Main.css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
        <img src="Images/Northwind.jpg" alt="Northwinds" />
        <div>
            <asp:GridView ID="gvProductMaintenance" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataSourceID="odsNorthwinds" PageSize="15" Width="699px" CellPadding="4" ForeColor="#333333" GridLines="None" OnRowCancelingEdit="gvProductMaintenance_RowCancelingEdit" OnRowUpdated="gvProductMaintenance_RowUpdated">
                <AlternatingRowStyle BackColor="White" />
                <Columns>
                    <asp:TemplateField HeaderText="ProductId" SortExpression="ProductId">
                        <EditItemTemplate>
                            <asp:Label ID="Label1" runat="server" Text='<%# Eval("ProductId") %>'></asp:Label>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label3" runat="server" Text='<%# Bind("ProductId") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="ProductName" SortExpression="ProductName">
                        <EditItemTemplate>
                            <asp:TextBox ID="tbProductName" runat="server" Text='<%# Bind("ProductName") %>'></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvProductName" runat="server" ControlToValidate="tbProductName" CssClass="error" Display="Dynamic" ErrorMessage="Please enter value into this field">*</asp:RequiredFieldValidator>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label1" runat="server" Text='<%# Bind("ProductName") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="UnitsInStock" SortExpression="UnitsInStock">
                        <EditItemTemplate>
                            <asp:TextBox ID="tbUnitInStock" runat="server" Text='<%# Bind("UnitsInStock") %>'></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvUnitsInStock" runat="server" ControlToValidate="tbUnitInStock" CssClass="error" Display="Dynamic" ErrorMessage="Please enter value into this field">*</asp:RequiredFieldValidator>
                            <asp:CompareValidator ID="cvUnitsInStock" runat="server" ControlToValidate="tbUnitInStock" CssClass="error" Display="Dynamic" ErrorMessage="Please enter value greater than or equal to 0" Operator="GreaterThanEqual" Type="Integer" ValueToCompare="0">*</asp:CompareValidator>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label2" runat="server" Text='<%# Bind("UnitsInStock") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:CommandField ShowEditButton="True" />
                </Columns>
                <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
                <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
                <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                <SortedAscendingCellStyle BackColor="#FDF5AC" />
                <SortedAscendingHeaderStyle BackColor="#4D0000" />
                <SortedDescendingCellStyle BackColor="#FCF6C0" />
                <SortedDescendingHeaderStyle BackColor="#820000" />
            </asp:GridView>
            <asp:ObjectDataSource ID="odsNorthwinds" runat="server" OldValuesParameterFormatString="original_{0}" SelectMethod="GetProducts" TypeName="ProductDb" UpdateMethod="UpdateProduct" OnUpdated="odsNorthwinds_Updated">
                <UpdateParameters>
                    <asp:Parameter Name="originalProduct" Type="Object" />
                    <asp:Parameter Name="product" Type="Object" />
                </UpdateParameters>
            </asp:ObjectDataSource>
        </div>
            <asp:Label ID="lblError" runat="server" CssClass="error"></asp:Label>
            <asp:ValidationSummary ID="vsProducts" CssClass="error" runat="server" HeaderText="Please correct the following issues:" />
        </form>
</body>
</html>
﻿using System;

/// <summary>
/// Summary description for Product
/// </summary>
/// <author>
/// Michael Morgaurge
/// </author>
/// <version>
/// 1.0
/// </version>
public class Product
{
    private string _productId;
    private string _productName;
    private Int16 _unitsInStock;

    /// <summary>
    /// Gets or sets the product identifier.
    /// </summary>
    /// <value>
    /// The product identifier.
    /// </value>
    /// <exception cref="Exception">Invalid value for the Product ID</exception>
    public string ProductId
    {
        get { return this._productId; }

        set
        {
            if (String.IsNullOrEmpty(value))
            {
                throw new Exception("Invalid value for the Product ID");
            }

            this._productId = value;
        }
    }

    /// <summary>
    /// Gets or sets the name of the product.
    /// </summary>
    /// <value>
    /// The name of the product.
    /// </value>
    /// <exception cref="Exception">Invalid value for the Product Name</exception>
    public string ProductName
    {
        get { return this._productName; }

        set
        {
            if (String.IsNullOrEmpty(value))
            {
                throw new Exception("Invalid value for the Product Name");
            }

            this._productName = value;
        }
    }

    /// <summary>
    /// Gets or sets the units in stock.
    /// </summary>
    /// <value>
    /// The units in stock.
    /// </value>
    /// <exception cref="Exception">Invalid value for the Units in Stock</exception>
    public Int16 UnitsInStock
    {
        get { return this._unitsInStock; }

        set
        {
            if (value < 0)
            {
                throw new Exception("Invalid value for the Units in Stock");
            }

            this._unitsInStock = value;
        }
    }
    
}
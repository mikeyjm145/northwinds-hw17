﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.IO;

/// <summary>
/// Summary description for ProductDB
/// </summary>
/// <author>
/// Michael Morguarge
/// </author>
/// <version>
/// 1.0
/// </version>
[DataObject(true)]
public class ProductDb
{
    /// <summary>
    /// Gets the products.
    /// </summary>
    /// <returns>List of products</returns>
    [DataObjectMethod(DataObjectMethodType.Select)]
    public static List<Product> GetProducts()
    {
        var oleDbConnection = new OleDbConnection(GetConnectionString());
        const string oleDbSelectStatement =
            "SELECT [ProductID], [ProductName], [UnitsInStock] " +
            "FROM [tblProducts] " +
            "ORDER BY [ProductID]";

        var oleDbCommand = new OleDbCommand(oleDbSelectStatement, oleDbConnection);

        oleDbConnection.Open();
        var oleDbDataReader = oleDbCommand.ExecuteReader(CommandBehavior.CloseConnection);

        if (oleDbDataReader == null)
        {
            throw new InvalidDataException("Could not execute query.");
        }

        List<Product> productsList = GenerateProductsList(oleDbDataReader);
        oleDbDataReader.Close();

        return productsList;
    }

    /// <summary>
    /// Generates the products list.
    /// </summary>
    /// <param name="oleDbDataReader">The OLE database data reader.</param>
    /// <returns>List of products generated from the data reader's data</returns>
    private static List<Product> GenerateProductsList(IDataReader oleDbDataReader)
    {
        var products = new List<Product>();
        while (oleDbDataReader.Read())
        {
            var currentProduct = new Product()
                {
                    ProductId = oleDbDataReader["ProductID"].ToString(),
                    UnitsInStock = Convert.ToInt16(oleDbDataReader["UnitsInStock"]),
                    ProductName = oleDbDataReader["ProductName"].ToString()
                };

            products.Add(currentProduct);
        }

        return products;
    }

    /// <summary>
    /// Updates the product.
    /// </summary>
    /// <param name="originalProduct">The original product.</param>
    /// <param name="product">The product.</param>
    /// <returns></returns>
    [DataObjectMethod(DataObjectMethodType.Update)]
    public static int UpdateProduct(Product originalProduct, Product product)
    {
        var oleDbConnection = new OleDbConnection(GetConnectionString());

        const string oleDbUpdateStatement =
            "UPDATE [tblProducts] " +
            "SET [ProductName] = @ProductName, [UnitsInStock] = @UnitsInStock " +
            "WHERE [ProductID] = @original_ProductID " +
            "AND [ProductName] = @original_ProductName " +
            "AND [UnitsInStock] = @orginal_UnitsInStock";
        var command = new OleDbCommand(oleDbUpdateStatement, oleDbConnection);

        command.Parameters.AddWithValue("ProductName",
            product.ProductName);
        command.Parameters.AddWithValue("UnitsInStock",
            product.UnitsInStock);

        command.Parameters.AddWithValue("original_ProductID",
            originalProduct.ProductId);
        command.Parameters.AddWithValue("original_ProductName",
            originalProduct.ProductName);
        command.Parameters.AddWithValue("orginal_UnitsInStock",
            originalProduct.UnitsInStock);

        oleDbConnection.Open();
        var updateCount = command.ExecuteNonQuery();
        oleDbConnection.Close();

        return updateCount;
    }

    /// <summary>
    /// Gets the connection string.
    /// </summary>
    /// <returns>The product database connection string</returns>
    private static string GetConnectionString()
    {
        return ConfigurationManager
            .ConnectionStrings["ProductMaintenanceConnectionString"].ConnectionString;
    }
}
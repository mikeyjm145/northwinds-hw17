﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;

/// <summary>
/// Code behind for ProductMaintenance page
/// </summary>
/// <author>
/// Michael Morguarge
/// </author>
/// <version>
/// 1.0
/// </version>
public partial class ProductMaintenance : Page
{
    /// <summary>
    /// Handles the Load event of the Page control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    /// <summary>
    /// Handles the RowUpdated event of the gvProductMaintenance control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="GridViewUpdatedEventArgs"/> instance containing the event data.</param>
    protected void gvProductMaintenance_RowUpdated(object sender, GridViewUpdatedEventArgs e)
    {
        if (e.Exception != null)
        {
            this.lblError.Text = "A database method has occurred.<br/>" +
                                 "Message: " + e.Exception.Message;

            if (e.Exception.InnerException != null)
            {
                this.lblError.Text += "<br />Message: "
                                      + e.Exception.InnerException.Message;
            }
            e.ExceptionHandled = true;
            e.KeepInEditMode = true;
        }
        else if (e.AffectedRows == 0)
        {
            this.lblError.Text = "Another user may have updated that category."
                                 + "<br />Please try again.";
        }
        else
        {
            this.lblError.Text = "";
        }
}

    /// <summary>
    /// Handles the RowCancelingEdit event of the gvProductMaintenance control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="GridViewCancelEditEventArgs"/> instance containing the event data.</param>
    protected void gvProductMaintenance_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        this.lblError.Text = "";
    }

    /// <summary>
    /// Handles the Updated event of the odsNorthwinds control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="ObjectDataSourceStatusEventArgs"/> instance containing the event data.</param>
    protected void odsNorthwinds_Updated(object sender, ObjectDataSourceStatusEventArgs e)
    {
        e.AffectedRows = Convert.ToInt32(e.ReturnValue);
    }
}